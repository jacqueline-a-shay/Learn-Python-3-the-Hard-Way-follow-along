# ths one is like your scripts with argv

def print_two(*args):
    arg1, arg2 = args # args is a lot like argv parameter, but args is for functions, has to go inside parentheses to work
    print(f"arg1: {arg1}, arg2: {arg2}")

# ok that *args is actually pointless, we can just do this
def print_two_again(arg1, arg2):
    print(f"arg1: {arg1}, arg2: {arg2}")

# this just takes one argument
def print_one(arg1):
    print(f"arg1: {arg1}")

# this one takes no arguments
def print_none():
    print("I got nothing")

print_two("Zed", "Shaw")
print_two_again("Zed", "Shaw")
print_one("First!")
print_none()

# *args 
# * means take ALL the arguments to the functions
# put them in args as a list
# it's like argv that you've been using, but for functions
# not used very common unless specifically needed