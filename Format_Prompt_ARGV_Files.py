# # # use argv and input together

from sys import argv
script, user_name = argv 
# # # need to supply "user_name" to run the program, 
# # # aka "$ python Learn_Python_3_the_Hard_Way.py Casey" will run the code using Casey as the user_name
prompt = '>'# this can be anything, when changed then the char showed in front of your answer space for your user will change accordingly
# # # prompt = '!' will have screen like follow
# # # Do you like me Casey?
# # #! ya
# # # prompt = '>'
# # # Do you like me Casey?
# # #> ya

print(f"(Hi {user_name}, I'm the {script} script.")
print("I'd like to ask you a few questions.")
print(f"Do you like me {user_name}?")
likes = input(prompt)

print(f"Where do you live {user_name}?")
lives = input(prompt)

print("What kind of computer do you have?")
computer = input(prompt)

print("What kind of pet do you have?")
pet = input(prompt)

print(f"""
Alright, so you said {likes} about liking me.

You live in {lives}. Not sure where that is.

And you have a {computer} computer. Nice!

Your pet is {pet}. How cute!
""")

# # reading file
# read file 'r+' write file 'w+'
# both read and write 'a+'
# # instead of hard coding into our code
# # use argv and input to ask user what file to open 

from sys import argv # from sys package import argv module
script, filename = argv # when wanting to run the python program, script will be the file_name.py part and filename will be the filename.txt
txt = open(filename) # this will open up the file you passed in when you run the code

print(f"Here's your file {filename}:")
print(txt.read()) # read the file and print out the content of the file

print(f"Type the filename again:") # prompt filename again so we can reprint or even give the client the oppurtunity to prompt for another file if they'd prefer
file_again = input("> ")

txt_again = open(file_again)
print(txt_again.read())


# some practice with files, using .json instead

import json
from sys import argv
script, user_name = argv
prompt = '>'
Name = input("What is your name? ")
print("Hi! {}". format(Name))

print("Would you like to create an account with us?")
consent = input(prompt)

print("Cool!  Let's get you started!")
print("Let's start with a user_id, you can choose from upper and lower case alphabets, numbers, and '_' =-)")
user_id = input(prompt)
file_name = user_id + '.json'

print("Would you also like to make your first deposit?")
def first_time_acct_trans():
    transaction_establishment ={}
    transaction_establishment["user_id"] = user_id
    print("Enter amount: ")
    deposit = input(prompt)
    deposit = float(deposit)
    transaction_establishment['deposit'] = deposit
    print("You've deposited ${:.2f} with your new account {}".format(deposit, user_id))
    return [transaction_establishment]
# first_time_acct_trans()
# print('Would you like to modify any information?')
# double_check = input(prompt)
# def ask_consent():
#     if double_check.lower() == 'yes':
#         print("Thank you!  We will process your account.")
#     else:
#         print("Sorry, let's make it right.")
# ask_consent(double_check)
# if ask_consent(double_check) 
#         first_time_acct_trans()

data = first_time_acct_trans()

with open(file_name, "w") as jsonFile:
    json.dump(data,jsonFile)



    # print("Enter category: ")
    # category = input(prompt)
    # if category == '':
    #     category = 'null'
    # transaction_establishment['category'] = category
    # print("Enter store: ")
    # store = input(store)
    # if store == '':
    #     store = 'null'
    # transaction_establishment['store'] = store

from sys import argv
script, filename = argv
print(f"We are going to erase {filename}.")
print("If you don't want that, hit CTRL-C (^C)")
print("If you do want that, hit RETURN")

input("?")

print("Opening the file...")
target = open(filename, "w")

print("Truncating the file. Goodbye") # empties the file, no going back!
target.truncate()

print("Now I'm going to ask you for three lines.")

line1 = input("line 1: ")
line2 = input("line 2: ")
line3 = input("line 3: ")

print("I'm going to write these to the file.")

target.write(line1)
target.write("\n")
target.write(line2)
target.write("\n")
target.write(line3)
target.write("\n")

print("And finally , we close it.")
target.close()

# seek(0) moves the read/write location to the beginning of the file
from sys import argv
script, filename = argv
print(f"We are going to erase {filename}.")
print("If you don't want that, hit CTRL-C (^C)")
print("If you do want that, hit RETURN")

input("?")

print("Opening the file...")
target = open(filename, "w")

print("Truncating the file. Goodbye") # empties the file, no going back!
target.truncate()

print("Now I'm going to ask you for three lines.")

line1 = input("line 1: ")
line2 = input("line 2: ")
line3 = input("line 3: ")

print("I'm going to write these to the file.")

target.write(line1 + '\n' + line2 + '\n' + line3)
skip = "\n"
print(f"""
The first line is: {line1} {skip}
The second line is: {line2}{skip}
And the third line is: {line3}{skip}
""")


print("And finally , we close it.")
target.close()


from sys import argv
from os.path import exists

script, from_file, to_file = argv

print(f"Copying from {from_file} to {to_file}")

# we could do these two on one line, how?
in_file = open(from_file)
indata = in_file.read()

print(f"The input file is {len(indata)} bytes long")

print(f"Does the output file exist? {exists(to_file)}")
print(f"Ready, hit RETURN to continue, CTRL-C to abort")
input()

out_file = open(to_file, 'w')
out_file.write(indata)

print("Alright, all done.")

out_file.close()
in_file.close()


