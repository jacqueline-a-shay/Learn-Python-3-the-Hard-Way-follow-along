# Write a function named isnegative. 
# It should accept a number and return a boolean value based on whether the input is negative.

def isnegative(n):
    """  
    >>> from solutions import isnegative
    >>> type(isnegative(0))
    <class 'bool'>
    >>> isnegative(4)
    False
    >>> isnegative(0)
    False
    >>> isnegative(-6)
    True
    """
    if int(n) < 0:
        return True
    else:
        return False
# R -    
def isnegative(x):
    return x < 0

# Write a function named count_evens. It should accept a list and return the number of odd numbers in the list.
def count_evens(list):
    """
    >>> from solutions import count_evens
    >>> type(count_evens([1, 2, 3]))
    <class 'int'>
    >>> count_evens([1, 2, 3])
    1
    >>> count_evens([4, 6, 8, 10, 12])
    5
    >>> count_evens([1, 3, 5, 7, 9])
    0
    >>> count_evens([])
    0
    >>> count_evens([3, 2])
    1
    """
    x = [i for i in list if i % 2 == 0]
    y = len(x)
    return y

# R -
# count
# check something even

def count_evens(numbers):
    count =  0
    for number in numbers:
        if number % 2 == 0:
            count += 1
    return count # not to count every counting event, so return to the layer before "if"


# Write a function named increment_odds. 
# It should accept a list of numbers and return a new list with the odd numbers from the original list incremented.

def increment_odds(numbers):
    """
    >>> from solutions import increment_odds
    >>> type(increment_odds([1, 2, 3]))
    <class 'list'>
    >>> increment_odds([1, 2, 3])
    [2, 2, 4]
    >>> increment_odds([2, 2, 1, 4, 5])
    [2, 2, 2, 4, 6]
    >>> increment_odds([])
    []
    >>> increment_odds([0, 1])
    [0, 2]"""
    r = []
    for i in numbers:
        if i % 2 != 0:
            r.append(i+1)
        elif i % 2 == 0:
            r.append(i)
    # r = list(map((lambda n: n + 1 for n in numbers if n % 2 != 0), numbers))
    return r
# - R
def increment_odds(numbers):
    """
    >>> from solutions import increment_odds
    >>> type(increment_odds([1, 2, 3]))
    <class 'list'>
    >>> increment_odds([1, 2, 3])
    [2, 2, 4]
    >>> increment_odds([2, 2, 1, 4, 5])
    [2, 2, 2, 4, 6]
    >>> increment_odds([])
    []
    >>> increment_odds([0, 1])
    [0, 2]"""
    output = []
    for number in numbers:
        # ask a q get an answer, mostly you use IF
        if number % 2 != 0:
            number += 1
        output.append(number)
    return output



# Write a function named average. It should accept a list of numbers and return the mean of the numbers.

def average(numbers):
    """   
    >>> from solutions import average
    >>> type(average([1, 2, 3]))
    <class 'float'>
    >>> average([1, 2, 3])
    2.0
    >>> average([4, 6, 8, 10, 12])
    8.0
    >>> average([1, 2])
    1.5"""
    x = sum(numbers)/len(numbers)
    return float(x)

# Create a function named name_to_dict. 
# It should accept a string that is a first name and last name separated by a space, and return a dictionary with first_name and last_name keys.
def name_to_dict(name):
    """
    >>> from solutions import name_to_dict
    >>> type(name_to_dict('Ada Lovelace'))
    <class 'dict'>
    >>> name_to_dict('Ada Lovelace')
    {'first_name': 'Ada', 'last_name': 'Lovelace'}
    >>> name_to_dict('Marie Curie')
    {'first_name': 'Marie', 'last_name': 'Curie'}"""
    parts = name.split(' ')
    first_name = parts[0]
    last_name = parts[1]

    # return {
    #     "first_name": first_name,
    #     "last_name": last
    # }

    full = {}
    full['first_name'] = first_name
    full['last_name'] = last_name
    return full



# Write a function named capitalize_names. 
# It should accept a list of dictionaries where each dictionary represents a person and has keys 
# first_name and last_name. It should return a list of dictionaries with each person's name capitalized.
def capitalize_names(names):
    
    """
    >>> from solutions import capitalize_names
    >>> names = []
    >>> names.append({'first_name': 'ada', 'last_name': 'lovelace'})
    >>> names.append({'first_name': 'marie', 'last_name': 'curie'})
    >>> names
    [{'first_name': 'ada', 'last_name': 'lovelace'}, {'first_name': 'marie', 'last_name': 'curie'}]
    >>> type(names)
    <class 'list'>
    >>> capitalize_names(names)
    [{'first_name': 'Ada', 'last_name': 'Lovelace'}, {'first_name': 'Marie', 'last_name': 'Curie'}]
    >>> type(capitalize_names(names))
    <class 'list'>"""
    names[0]['first_name'] = names[0]['first_name'].capitalize()
    names[0]['last_name'] = names[0]['last_name'].capitalize()
    names[1]['first_name'] = names[1]['first_name'].capitalize()
    names[1]['last_name'] = names[1]['last_name'].capitalize()
    return names

# - R
def capitalize_names(names):
    output = []
    for name in names:
        name["first_name"] = name["first_name"].capitalize()
        name["last_name"] = name["last_name"].capitalize()
        output.append(name)
    return output

# Write a function named count_vowels. 
# It should accept a word and return a number that is the number of vowels in the given word. "y" should not count as a vowel.
def count_vowels(word):
    """>>> from solutions import count_vowels
    >>> type(count_vowels('codeup'))
    <class 'int'>
    >>> count_vowels('codeup')
    3
    >>> count_vowels('abcde')
    2
    >>> count_vowels('why')
    0"""
    counter = 0
    vowels = set('aeiouAEIOU')
    for w in word:
        if w in vowels:
            counter += 1
        # for w in words:
        #   if w.lower() in ['a','e','i','o','u']: 
        #       count += 1
        # return count
    return counter

# Write a function named analyze_word. 
# It should accept a string that is a word and return a dictionary with information about the word, 
# the total number of characters in the word, the original word, and the number of vowels in the word.
def analyze_word(word):
    """
    >>> from solutions import analyze_word
    >>> type(analyze_word('codeup'))
    <class 'dict'>
    >>> analyze_word('codeup')
    {'word': 'codeup', 'n_letters': 6, 'n_vowels': 3}
    >>> analyze_word('abcde')
    {'word': 'abcde', 'n_letters': 5, 'n_vowels': 2}
    >>> analyze_word('why')
    {'word': 'why', 'n_letters': 3, 'n_vowels': 0}"""
    counter = 0
    vowels = set('aeiouAEIOU')
    for w in word:
        if w in vowels:
            counter += 1

    ana = {}
    ana['word'] = word
    ana['n_letters'] = len(word)
    ana['n_vowels'] = counter
    return ana

def analyze_word(word):
    output = {}
    output["word"] = word
    output["n_letters"] = len(word)
    output["n_vowels"] = count_vowels(word)
    return output







    